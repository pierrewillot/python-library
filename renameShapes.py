import maya.cmds as mc
def renameShapes():
	target = mc.ls(sl=1)
	if target:
		for x in target:
			shapes = mc.listRelatives(x, s=True)
			if shapes:
				if len(shapes) == 1:
					mc.rename(x+'|'+shapes[0], x+'Shape')
				else:
					i=0
					for y in shapes:
						mc.rename(x+'|'+y, x+'Shape'+str(i))
						i=i+1
	else:
		mc.warning('Select at least one object for renaming its shape(s).')