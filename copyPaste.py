# ************************************************************************************************************
# Title: copyPaste.py
# Author: Pierre WILLOT
# Last update: 07/06/2015
# Mail: willotpierre@yahoo.fr
# ************************************************************************************************************

# INSTALLATION:
# copy the file under C:\Users\USER\Documents\maya\VERSION\scripts folder
# import the script with the following python command:
# import copyPaste
# reload(copyPaste)

# IMPORT MODULES
import maya.cmds as cmds
from functools import partial

# window
def cp_win():
	win = "win_cp"
	copyButton = " Copy "
	pasteButton = " Paste "

	# delete window if exist
	if cmds.window(win, exists=True):
	    cmds.deleteUI (win)
	    cmds.windowPref(win, remove=True)

	cmds.window(win, s=False, bgc=(0.2,0.2,0.2), t= 'Copy Paste - Pierre Willot')
	cmds.columnLayout(adj=1)

	# translate
	cmds.rowLayout(nc=3)
	cmds.columnLayout(adj=1)
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'tx'))
	cmds.button(l=pasteButton, c=partial(paste_def,'tx'))
	cmds.textFieldGrp('tx_tf', cw2=(70,90), l='Translate X = ', tx= 0)
	cmds.checkBox('reverse_tx', ann='reverse tx', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'ty'))
	cmds.button(l=pasteButton, c=partial(paste_def,'ty'))
	cmds.textFieldGrp('ty_tf', cw2=(70,90), l='Translate Y = ', tx= 0)
	cmds.checkBox('reverse_ty', ann='reverse ty', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'tz'))
	cmds.button(l=pasteButton, c=partial(paste_def,'tz'))
	cmds.textFieldGrp('tz_tf', cw2=(70,90), l='Translate Z = ', tx= 0)
	cmds.checkBox('reverse_tz', ann='reverse tz', l='')
	cmds.setParent('..')
	cmds.setParent('..')
	cmds.button(l='c T', h=70, c=partial(copy_transf_def,'t'))
	cmds.button(l='p T', h=70, c=partial(paste_transf_def,'t'))
	cmds.setParent('..')
	cmds.setParent('..')

	# rotate
	cmds.rowLayout(nc=3)
	cmds.columnLayout(adj=1)
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'rx'))
	cmds.button(l=pasteButton, c=partial(paste_def,'rx'))
	cmds.textFieldGrp('rx_tf', cw2=(70,90), l='Rotate X = ', tx= 0)
	cmds.checkBox('reverse_rx', ann='reverse rx', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'ry'))
	cmds.button(l=pasteButton, c=partial(paste_def,'ry'))
	cmds.textFieldGrp('ry_tf', cw2=(70,90), l='Rotate Y = ', tx= 0)
	cmds.checkBox('reverse_ry', ann='reverse ry', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'rz'))
	cmds.button(l=pasteButton, c=partial(paste_def,'rz'))
	cmds.textFieldGrp('rz_tf', cw2=(70,90), l='Rotate Z = ', tx= 0)
	cmds.checkBox('reverse_rz', ann='reverse rz', l='')
	cmds.setParent('..')
	cmds.setParent('..')
	cmds.button(l='c R', h=70, c=partial(copy_transf_def,'r'))
	cmds.button(l='p R', h=70, c=partial(paste_transf_def,'r'))
	cmds.setParent('..')
	cmds.setParent('..')

	# scale
	cmds.rowLayout(nc=3)
	cmds.columnLayout(adj=1)
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'sx'))
	cmds.button(l=pasteButton, c=partial(paste_def,'sx'))
	cmds.textFieldGrp('sx_tf', cw2=(70,90), l='Scale X = ', tx= 1)
	cmds.checkBox('reverse_sx', ann='reverse sx', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'sy'))
	cmds.button(l=pasteButton, c=partial(paste_def,'sy'))
	cmds.textFieldGrp('sy_tf', cw2=(70,90), l='Scale Y = ', tx= 1)
	cmds.checkBox('reverse_sy', ann='reverse sy', l='')
	cmds.setParent('..')
	cmds.rowLayout(nc=4)
	cmds.button(l=copyButton, c=partial(copy_def,'sz'))
	cmds.button(l=pasteButton, c=partial(paste_def,'sz'))
	cmds.textFieldGrp('sz_tf', cw2=(70,90), l='Scale Z = ', tx= 1)
	cmds.checkBox('reverse_sz', ann='reverse sz', l='')
	cmds.setParent('..')
	cmds.setParent('..')
	cmds.button(l='c S', h=70, c=partial(copy_transf_def,'s'))
	cmds.button(l='p S', h=70, c=partial(paste_transf_def,'s'))
	cmds.setParent('..')
	cmds.button(h=30, l='Copy All', c=partial(copy_all_def))
	cmds.button(h=30, l='Paste All', c=partial(paste_all_def))

	# show window
	cmds.showWindow(win)

def copy_def(attr, *args):
	selection = cmds.ls(sl=True)
	if len(selection) == 0:
		if attr[0] == 's':
			cmds.textFieldGrp('{0}_tf'.format(attr), e=True, tx=1)
			print("{0} channel set to 1".format(attr))
		else:
			cmds.textFieldGrp('{0}_tf'.format(attr), e=True, tx=0)
			print("{0} channel set to 0".format(attr))
	else:
		cp_selection = selection[0]
		attr_value = cmds.getAttr('{0}.{1}'.format(cp_selection, attr))
		cmds.textFieldGrp('{0}_tf'.format(attr), e=True, tx=attr_value)
		print("{0} channel set to {1}".format(attr, attr_value))

def paste_def(attr, *args):
	cp_selection = cmds.ls(sl=True)
	if len(cp_selection) == 0:
		print ('Please select at least one object')
	else:
		cb_value = cmds.checkBox('reverse_{0}'.format(attr), q=True, v=True)
		if cb_value == 0:
			reverse_value = 1
		else:
			reverse_value = -1
		channel_value = cmds.textFieldGrp('{0}_tf'.format(attr), q=True, tx=True)
		for cp_selected in cp_selection:
			is_locked = cmds.getAttr('{0}.{1}'.format(cp_selected, attr), lock=True)
			if is_locked == True:
				print("{0}.{1} skipped because the attribute is locked".format(cp_selected, attr))
			else:		
				cmds.setAttr('{0}.{1}'.format(cp_selected, attr), (float(channel_value)*reverse_value))
				print("{0}.{1} set to {2}".format(cp_selected, attr, float(channel_value)*reverse_value))

def copy_transf_def(transf, *args):
	copy_def('{0}x'.format(transf))
	copy_def('{0}y'.format(transf))
	copy_def('{0}z'.format(transf))

def paste_transf_def(transf, *args):
	paste_def('{0}x'.format(transf))
	paste_def('{0}y'.format(transf))
	paste_def('{0}z'.format(transf))

def copy_all_def(*args):
	copy_transf_def('t')
	copy_transf_def('r')
	copy_transf_def('s')

def paste_all_def(*args):
	paste_transf_def('t')
	paste_transf_def('r')
	paste_transf_def('s')

cp_win()