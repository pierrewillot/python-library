import urllib
import maya.cmds as cm
from functools import partial  

def loadlinks(myLayout, *args):
    links = {
        "Anim Exporter" : "https://bit.ly/2Nuqfi3",
        "Copy Paste" : "https://bit.ly/2KhYjMz"
        }
    for link in links:
        cm.button(label=link, command=partial(readlink, links[link]))
            
def showUI():
    winID = "win"

    if cm.window(winID, exists=True):
      	cm.deleteUI(winID)

    cm.window(winID, title = "Pierre Willot / python library")

    scrollLayout = cmds.scrollLayout(cr=1)

    myLayout = cm.columnLayout(adjustableColumn=True)

    loadlinks(myLayout)
    cm.showWindow(winID)

def readlink(link, *args):
    f = urllib.urlopen(link)
    myfile = f.read()
    exec(myfile)
showUI()
