import maya.cmds as mc
from functools import partial
import csv, os

class AnimExport:
	def __init__(
					self,
					debug = True,
					export_path = os.environ["HOME"]+'/maya/scripts/output.csv', 
					import_path = os.environ["HOME"]+'/maya/scripts/output.csv', 
				):

		self.debug = debug
		self.export_path = export_path
		self.import_path = import_path
		self.window()
		self.fieldnames = ['Attribute', 'Frame', 'Value', 'Node', 'NameSpace', 'Weighted', 'PreInfinity', 'PostInfinity', 'InTangent', 'OutTangent', 'TanLock', 'WeightLock', 'BreakDown', 'InAngle', 'OutAngle', 'InWeight', 'OutWeight', 'LongName']

	def export_file(self):
		selection = mc.ls(sl=1)

		if selection:
			try:
				csvfile = open(self.export_path, "w")
			except:
				return mc.warning('The export failed, please close the file first if you want to override it.')

			listToCompute = []
			for sel in selection:
				for attr in mc.listAnimatable(sel):
					listToCompute.append(attr)

			sizeList = len(listToCompute)
			add = 100.0 /sizeList
			amount = 0
			mc.progressWindow(title='Please Wait ...', progress=amount, status='Checking: 0%', isInterruptable=1)

			try:
				if self.debug:
					print '\n>> Writing datas..\n'

				w = csv.DictWriter(csvfile, dialect=csv_dialect, fieldnames=self.fieldnames)
				w.writeheader()

				for sel in selection:

					if mc.progressWindow(query=1, isCancelled=1):
						break

					mc.select(sel, r=True)

					if ':' in sel:
						node = sel.split(':')[-1]
						nameSpace = sel.replace(node, '')

					else:
						node = sel
						nameSpace = ''

					longName = mc.ls(sl=True, l=True)[0]

					for attr in mc.listAnimatable(sel):

						attr = attr.split('.')[-1]

						isKey = mc.keyframe(sel, at=attr, q=True, timeChange=True)

						if isKey:
							preInfinity = mc.setInfinity(at=attr, q=True, pri=True)[0]
							postInfinity = mc.setInfinity(at=attr, q=True, poi=True)[0]

							weighted = mc.keyTangent(at=attr, q=True, weightedTangents=True)[0]

							w.writerow({'Attribute': attr, 'PreInfinity': preInfinity, 'PostInfinity': postInfinity, 'Weighted': weighted, 'Node': node, 'LongName': longName, 'NameSpace': nameSpace})

							keys       = mc.keyframe  (sel, at=attr, q=True, timeChange=True)
							values     = mc.keyframe  (sel, at=attr, q=True, valueChange=True)
							breakDown  = mc.keyframe  (sel, at=attr, q=True, breakdown=True)
							inTangent  = mc.keyTangent(sel, at=attr, q=True, itt=True)
							outTangent = mc.keyTangent(sel, at=attr, q=True, ott=True)
							tanLock    = mc.keyTangent(sel, at=attr, q=True, lock=True)
							weightLock = mc.keyTangent(sel, at=attr, q=True, weightLock=True)
							inAngle    = mc.keyTangent(sel, at=attr, q=True, inAngle=True)
							outAngle   = mc.keyTangent(sel, at=attr, q=True, outAngle=True)
							inWeight   = mc.keyTangent(sel, at=attr, q=True, inWeight=True)
							outWeight  = mc.keyTangent(sel, at=attr, q=True, outWeight=True)

							for i in range(0, len(keys), 1):
								bd_value = 0
								if breakDown:
									for bd in breakDown:
										if bd == keys[i]:
											bd_value = 1

								if inTangent[i] != "fixed":
									inAngle[0]  = ''
									inWeight[0] = ''

								if outTangent[i] != "fixed":
									outAngle[0]  = ''
									outWeight[0] = ''

								w.writerow({'Frame' : keys[i], 'Value' : values[i], 'BreakDown' : bd_value, 'InTangent' : inTangent[i], 'OutTangent' : outTangent[i],'TanLock' : tanLock[i], 'WeightLock' : weightLock[i], 'InAngle' : inAngle[i], 'InWeight' : inWeight[i], 'OutAngle' : outAngle[i], 'OutWeight' : outWeight[i]})

						elif not isKey:
							value = mc.getAttr(longName+'.'+attr)
							w.writerow({'Attribute': attr, 'Node': node, 'LongName': longName, 'NameSpace': nameSpace})
							w.writerow({'Frame' : 'Static', 'Value' : value})

						if amount < 100:
							amount += round(add, 0)
						else:
							mc.progressWindow(endProgress=1)

						mc.progressWindow(edit=1, progress=amount, status='Export -> {0}%'.format(amount))
						
			finally:
				mc.progressWindow(endProgress=1)
				csvfile.close()
				mc.select(selection, r=True)

				if self.debug:
					print '\n>> Done writing datas.\n'

		else:
			mc.warning('/!\ Please select at least an object /!\ ')

	def import_file(self):
		selection = mc.ls(sl=True)

		if selection:
			with open(self.import_path, 'rb') as f:
				reader = csv.reader(f)

				_attribute = self.fieldnames.index('Attribute')
				_longName  = self.fieldnames.index('LongName')
				_frame     = self.fieldnames.index('Frame')
				_value     = self.fieldnames.index('Value')
				_tanLock   = self.fieldnames.index('TanLock')

				dataList = []
				for r in reader:
					dataList.append(r[0].split(csv_dialect.delimiter))

				attribute = ''
				longName  = ''
				for x in range (1, len(dataList)):
					line = dataList[x]

					if line[_attribute] != '':
						attribute = line[_attribute]
						longName  = line[_longName]
	
					else:
						frame   = line[_frame]

						if frame == 'Static':
							if line[_value] == 'False':
								value = 0.0
							elif line[_value] == 'True':
								value = 1.0
							else:
								value   = float(line[_value])
							mc.setAttr(longName + '.' + attribute, value)

						else:
							value   = float(line[_value])
							tanLock = bool(line[_tanLock])
							frame = float(frame)
							mc.setKeyframe(longName, at=attribute, t=frame, value=value)
							mc.keyTangent (longName, at=attribute, lock=tanLock, t=(frame, frame))
		else:
			mc.warning('/!\ Please select at least an object /!\ ')

	def window(self):
		if mc.window('win_Attr', exists=True):
			mc.deleteUI ('win_Attr')
			mc.windowPref('win_Attr', remove=True)
			
		window = mc.window('win_Attr', t='Anim Exporter - Pierre Willot')
		mc.columnLayout(adj=True)
		mc.frameLayout('Export', cll=False, cl=True)
		mc.rowLayout(nc=2, adj=True)
		mc.textFieldGrp('Export_filePathTFG', cw2=(50,250), l='File path:', tx=self.export_path)
		mc.button(l='Browse', c=partial(self.path_Export), w=50)
		mc.setParent('..')
		mc.button(l='EXPORT', c=partial(self.call_Export))
		mc.separator(h=20)
		mc.setParent('..')

		mc.frameLayout('Import', cll=False, cl=True)		
		mc.rowLayout(nc=2, adj=True)
		mc.textFieldGrp('Import_filePathTFG', cw2=(50,250), l='File path:', tx=self.import_path)
		mc.button(l='Browse', c=partial(self.path_Import), w=50)
		mc.setParent('..')
		mc.button(l='IMPORT', c=partial(self.call_Import))

		mc.showWindow('win_Attr')

	def path_Export(*args):
		path = mc.fileDialog2(cap = 'Load Export Path', ff="*.csv", dialogStyle=2, fm=0, okc='Load')[0]
		mc.textFieldGrp('Export_filePathTFG', edit=True, tx=path)
	
	def call_Export(self, *args):
		self.export_path = mc.textFieldGrp('Export_filePathTFG', query=True, tx=True)
		self.export_file()

	def path_Import(*args):
		path = mc.fileDialog2(cap = 'Load Import Path', ff="*.csv", dialogStyle=2, fm=1, okc='Load')[0]
		mc.textFieldGrp('Import_filePathTFG', edit=True, tx=path)

	def call_Import(self, *args):
		self.import_path = mc.textFieldGrp('Export_filePathTFG', query=True, tx=True)
		self.import_file()

class csv_dialect:
	delimiter = ';'

AnimExport()